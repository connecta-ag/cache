<?php
namespace CAG\Cache\Cache\Backend;

/**
 * A caching backend which stores cache entries by using Redis with phpredis
 * PHP module. Redis is a noSQL database with very good scaling characteristics
 * in proportion to the amount of entries and data size.
 *
 * @see http://code.google.com/p/redis/
 * @see http://github.com/owlient/phpredis
 * @api
 */
class RedisBackend extends \TYPO3\CMS\Core\Cache\Backend\RedisBackend
{
    /**
     * Get tags to given cache identifier
     *
     * @param $entryIdentifier
     * @return array
     */
    public function getTagsByIdentifier($entryIdentifier)
    {
        if (!$this->canBeUsedInStringContext($entryIdentifier)) {
            throw new \InvalidArgumentException('The specified identifier is of type "' . gettype($entryIdentifier) . '" which can\'t be converted to string.', 1377006654);
        }
        $assignedTags = [];
        if ($this->connected) {
            if ($this->redis->exists(self::IDENTIFIER_DATA_PREFIX . $entryIdentifier)) {
                $assignedTags = $this->redis->sMembers(self::IDENTIFIER_TAGS_PREFIX . $entryIdentifier);
            }
        }
        return $assignedTags;
    }
}
