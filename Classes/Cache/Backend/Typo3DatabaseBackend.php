<?php
namespace CAG\Cache\Cache\Backend;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Core\Cache\Exception;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * A caching backend which stores cache entries in database tables
 * @api
 */
class Typo3DatabaseBackend extends \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend
{
    /**
     * Get tags to given cache identifier
     *
     * @param string $entryIdentifier
     * @return mixed
     */
    public function getTagsByIdentifier($entryIdentifier)
    {
        $tags = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            'tag',
            'cf_cache_pages_tags',
            'identifier = \'' . $entryIdentifier . '\''
        );

        return array_column($tags, 'tag');
    }
}
