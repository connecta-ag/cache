<?php
namespace CAG\Cache\Cache\Frontend;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Cache\Backend\TransientBackendInterface;

/**
 * A cache frontend for any kinds of PHP variables
 *
 * This file is a backport from FLOW3
 * @api
 */
class VariableFrontend extends \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend
{

    /**
     * Get tags to given cache identifier
     *
     * @param $entryIdentifier
     * @return array|bool
     */
    public function getCacheTagsByIdentifier($entryIdentifier)
    {
        if (!$this->isValidEntryIdentifier($entryIdentifier)) {
            throw new \InvalidArgumentException('"' . $entryIdentifier . '" is not a valid cache entry identifier.', 1233058294);
        }

        if(method_exists($this->backend, 'getTagsByIdentifier')) {
            $rawResult = $this->backend->getTagsByIdentifier($entryIdentifier);
        }else {
            $rawResult = false;
        }

        if ($rawResult === false) {
            return false;
        } else {
            return $rawResult;
        }
    }
}
