<?php
namespace CAG\Cache\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * PersonController
 */
class BackendController extends \CAG\Cache\Controller\AbstractController
{

    /** @var \TYPO3\CMS\Core\Database\DatabaseConnection */
    protected $databaseConnection;

    /**
     * Initializes the class.
     */
    public function __construct()
    {
        parent::__construct();

        $this->databaseConnection = $GLOBALS['TYPO3_DB'];
    }

    /**
     * action list
     */
    public function overviewAction()
    {
    }

    /**
     * action invalidatePath
     */
    public function invalidatePidAction()
    {
        $invalidationService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstanceService('cache', 'cacheInvalidation');

        if ($this->request->hasArgument('pid')) {
            $pid = $this->request->getArgument('pid');
            $invalidationService->clearCacheByPid($pid);
        }
        $this->redirect('overview');
    }

    /**
     * action invalidatePathRecursiveAction
     */
    public function invalidatePathByFastPurgeAction()
    {
        $invalidationService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstanceService('cache_invalidation', 'cacheInvalidation');

        if ($this->request->hasArgument('paths') && !empty(trim($this->request->getArgument('paths')))) {
            $pathsString = $this->request->getArgument('paths');
            $pathsString = str_replace("\r", '', $pathsString);
            $paths = explode("\n", $pathsString);
            $invalidationObjects = $invalidationService->clearCacheByPaths($paths, false, true);
            $this->view->assignMultiple([
                'fastPurgeInvalidationObjects' => $invalidationObjects
            ]);
        }
    }

    /**
     * action invalidatePathRecursiveAction
     */
    public function invalidatePathRecursiveAction()
    {
        $invalidationService = GeneralUtility::makeInstance('CAG\\Cache\\Service\\InvalidationService');

        $paths = [];
        if ($this->request->hasArgument('assets')) {
            $paths = $this->request->getArgument('assets');
        }
        if ($this->request->hasArgument('path') && !empty(trim($this->request->getArgument('path')))) {
            $paths[] = trim($this->request->getArgument('path'));
        }
        $invalidationService->clearCacheByPaths($paths);
        $this->redirect('overview');
    }

    /**
     * Fetches records from the database by the field name. This is a replacement for the
     * BackendUtility::getRecordsByField() method, which is deprecated since TYPO3 8.7.
     *
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $fieldValue
     * @return array
     */
    protected function getRecordsByField($tableName, $selectFields, $fieldName, $fieldValue)
    {
        $rows = $this->databaseConnection->exec_SELECTgetRows(
            $selectFields,
            $tableName,
            $fieldName . '=' . $this->databaseConnection->fullQuoteStr($fieldValue, $tableName)
        );

        return (array)$rows;
    }
}
