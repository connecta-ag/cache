<?php
namespace CAG\Cache\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Person
 */
class InvalidationItem extends InvalidationInterface
{
    const INVALIDATION_HEADER_PAGE_ID = 'X-CACHE-PID';
    const INVALIDATION_HEADER_PAGE_PATH = 'X-CACHE-URL';
    const INVALIDATION_HEADER_PAGE_PATH_RECURSIVE = 'X-CACHE-URL-RECURSIVE';
    const INVALIDATION_HEADER_TAG = 'X-CACHE-TAGS';

    /**
     * pageId
     *
     * @var string
     */
    protected $pageId = null;

    /**
     * pagePath
     *
     * @var string
     */
    protected $pagePath = '';

    /**
     * tag
     *
     * @var string
     */
    protected $tag = '';

    /**
     * recursive
     *
     * @var string
     */
    protected $recursive = false;

    public function __toString()
    {
        $invalidationDescriptor = '';
        switch ($this->getInvalidationHeadeName()) {
            case self::INVALIDATION_HEADER_PAGE_ID:
                $invalidationDescriptor = $this->pageId . '';
                break;
            case self::INVALIDATION_HEADER_PAGE_PATH:
            case self::INVALIDATION_HEADER_PAGE_PATH_RECURSIVE:
                $invalidationDescriptor = $this->pagePath;
                break;
            case self::INVALIDATION_HEADER_TAG:
                $invalidationDescriptor = $this->tag;
                break;
        }
        return $invalidationDescriptor;
    }

    /**
     * @return string
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getPagePath()
    {
        return $this->pagePath;
    }

    /**
     * @param string $pagePath
     */
    public function setPagePath($pagePath)
    {
        $this->pagePath = $pagePath;
    }

    /**
     * @return string
     */
    public function getRecursive()
    {
        return $this->recursive;
    }

    /**
     * @param string $recursive
     */
    public function setRecursive($recursive)
    {
        $this->recursive = $recursive;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /*
     * Helper
     */

    public function getInvalidationHeadeName()
    {
        if (!empty($this->pageId)) {
            return self::INVALIDATION_HEADER_PAGE_ID;
        }
        if (!empty($this->pagePath)) {
            return $this->recursive ? self::INVALIDATION_HEADER_PAGE_PATH_RECURSIVE : self::INVALIDATION_HEADER_PAGE_PATH;
        }
        if (!empty($this->tag)) {
            return self::INVALIDATION_HEADER_TAG;
        }
    }

    /**
     * @return string
     */
    public function getInvalidationRegexp()
    {
        return strval(strval($this));
    }
}
