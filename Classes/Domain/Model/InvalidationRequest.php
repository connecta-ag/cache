<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace CAG\Cache\Domain\Model;

class InvalidationRequest
{
    protected $request;
    protected $requestUri;
    protected $responseCode;
    protected $responseMessage;

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @param mixed $responseCode
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    }

    public function getResponseCode()
    {
        if (empty($this->responseCode) && !empty($this->request)) {
            $this->responseCode = curl_getinfo($this->request, CURLINFO_HTTP_CODE);
        }
        return $this->responseCode;
    }

    /**
     * @param mixed $responseMessage
     */
    public function setResponseMessage($responseMessage)
    {
        $this->responseMessage = $responseMessage;
    }

    public function getResponseMessage()
    {
        if (empty($this->responseMessage) && !empty($this->request)) {
            if ($this->getResponseCode() != 200) {
                $this->responseMessage = curl_error($this->request);
            } else {
                $this->responseMessage = curl_multi_getcontent($this->request);
            }
        }
        return $this->responseMessage;
    }

    /**
     * @return mixed
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }

    /**
     * @param mixed $requestUri
     */
    public function setRequestUri($requestUri)
    {
        $this->requestUri = $requestUri;
    }

    /*
     * HELPER
     */

    public function getSuccess()
    {
        $responseCode = $this->getResponseCode();
        return $responseCode == 200 || $responseCode == 201;
    }
}
