<?php
namespace CAG\Cache\Hooks;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team <typo3@connecta.ag>, Connecta AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CAG\Cache\Service\InvalidationService;

/**
 *
 */
class DataHandler
{
    public function processDatamap_afterDatabaseOperations($status, $table, $id, array $fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if ($status == 'update') {
            /** @var InvalidationService $invalidationService */
            $invalidationService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstanceService('cache_invalidation', 'cacheInvalidation');
            $tag = $table . '_' . $id;
            $invalidationService->clearCacheByTags([$tag]);
        }
    }
}
