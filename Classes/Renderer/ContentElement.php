<?php
namespace CAG\Cache\Renderer;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class ContentElement
{

    /**
     * @var ContentObjectRenderer
     */
    public $cObj;

    /**
     * @return string
     */
    public function render()
    {
        if (($identifier = GeneralUtility::_GET('identifier')) && ($key = GeneralUtility::_GET('key'))) {
            if ($row = $this->getCacheManager()->get($identifier)) {
                /* @var $INTiS_cObj ContentObjectRenderer */
                $INTiS_cObj = unserialize($row['cache_data']['INTincScript'][$key]['cObj']);
                $INTiS_cObj->INT_include = 1;
                $this->addCacheHeaders(0);
                return $INTiS_cObj->cObjGetSingle($row['cache_data']['INTincScript'][$key]['type'] . '_INT', $row['cache_data']['INTincScript'][$key]['conf']);
            }
        }

        if (!($cUid = GeneralUtility::_GET('element'))) {
            return '';
        }

        $configArray = [
                'tables' => 'tt_content',
                'source' => intval($cUid),
                'dontCheckPid' => 1,
        ];

        $ceRecord = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tt_content', intval($cUid), 'esi_cache_duration');

        // default no cache (0 seconds)
        $cacheDuration = intval($ceRecord['esi_cache_duration']) < 0 ? 0 : intval($ceRecord['esi_cache_duration']);
        $this->addCacheHeaders($cacheDuration);
        return $this->cObj->cObjGetSingle('RECORDS', $configArray);
    }

    /**
     * @param $cacheDuration integer Cache duration in seconds
     */
    private function addCacheHeaders($cacheDuration)
    {
        // set cache time of esi content
        $cacheExpires = $GLOBALS['EXEC_TIME'] + $cacheDuration;
        $headers = [
            'Expires: ' . gmdate('D, d M Y H:i:s T', $cacheExpires),
            'Cache-Control: max-age=' . $cacheDuration,
            // no-cache
            'Pragma: public'
        ];

        // Send headers:
        foreach ($headers as $hL) {
            header($hL);
        }
    }

    /**
     * @return VariableFrontend
     */
    protected function getCacheManager()
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('cache_pages');
    }
}
