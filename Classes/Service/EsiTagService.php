<?php
namespace CAG\Cache\Service;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Class EsiTagService
 */
class EsiTagService
{

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObjectRenderer;

    /**
     * @var \CAG\Cache\Service\TyposcriptPluginSettingsService
     * @inject
     */
    protected $typoscriptPluginSettingsService;


    /**
     * Render content and add esi tags (if function is activate)
     *
     * @param $content
     * @param ContentObjectRenderer $contentObjectRenderer
     * @return string
     */
    public function render($content, ContentObjectRenderer $contentObjectRenderer)
    {
        $this->contentObjectRenderer = $contentObjectRenderer;
        $data = $this->contentObjectRenderer->data;

        $esiType = (integer) $data['enable_esi'];

        if ($esiType == 1) {
            $content = $this->renderVarnish($content);
        } else if ($esiType == 2) {
            $content = $this->renderAjax($content);
        }

        return $content;
    }


    /**
     * Render content and add esi tags (if function is activate)
     *
     * @param $content
     * @return string
     */
    protected function renderVarnish($content)
    {

        $typoScriptConfig = $this->typoscriptPluginSettingsService->getConfiguration();

        $esiContent = '';

        $additionalParams = GeneralUtility::_GET();
        $additionalParams['varnish'] = 1;


        if (!empty($typoScriptConfig['replaceIntWithEsi']) && $this->isIntObject($content)) {
            // replace all esi ce objects with esi marker

            $additionalParams['type'] = $typoScriptConfig['typeNum'];
            $additionalParams['identifier'] = $GLOBALS['TSFE']->newHash;
            $additionalParams['key'] = $this->getKey($content);

            $link = $this->getLink($additionalParams);
            $esiContent = $this->wrapEsiTag($link);

        } elseif ($this->contentObjectRenderer->data['enable_esi'] && $GLOBALS['TSFE']->type != $typoScriptConfig['esiTypeNum']) {
            // add possibility to exclude element from page caching and render it by an additional request

            $additionalParams['type'] = $typoScriptConfig['esiTypeNum'];
            $additionalParams['element'] = $this->contentObjectRenderer->data['uid'];
            $link = $this->getLink($additionalParams);
            $esiContent = $this->wrapEsiTag($link);
        }

        // render content for compatibility for use with non varnish setup
        // this content will be removed by varnish if esi is active
        if (!empty($typoScriptConfig['renderEsiRemoveTagWithOriginalContent']) && !empty($esiContent)) {
            $esiContent .= '<esi:remove>' . $content . '</esi:remove>';
        }

        if (!empty($esiContent)) {
            $content = $esiContent;
        }

        return $content;
    }

    /**
     * Render content and add esi tags (if function is activate)
     *
     * @param $content
     * @return string
     */
    protected function renderAjax($content)
    {
        $typoScriptConfig = $this->typoscriptPluginSettingsService->getConfiguration();


        $additionalParams = GeneralUtility::_GET();

        $additionalParams['ajax'] = 1;

//        if ($this->isIntObject($content)) {
//
//            $additionalParams['type'] = $typoScriptConfig['typeNum'];
//            $additionalParams['identifier'] = $GLOBALS['TSFE']->newHash;
//            $additionalParams['key'] = $this->getKey($content);
//            $link = $this->getLink($additionalParams);
//
//        } else {
            //$additionalParams['type'] = $typoScriptConfig['esiTypeNum'];
            $additionalParams['element'] = $this->contentObjectRenderer->data['uid'];

            $additionalParamsAsStr = '&' . http_build_query($additionalParams);


            // $link = $this->getLink($additionalParams);
//        }

        $esiContent = '<div data-js-action="load" data-type="' . (integer) $typoScriptConfig['esiTypeNum'] . '" data-params="' . $additionalParamsAsStr . '"></div>';

        if (!empty($esiContent)) {
            $content = $esiContent;
        }

        return $content;
    }


    protected function getLink($additionalParams)
    {

        $additionalParamsAsStr = '&' . http_build_query($additionalParams);

        $link = $this->contentObjectRenderer->typoLink_URL([
            'parameter' => $GLOBALS['TSFE']->id,
            'forceAbsoluteUrl' => 0,
            'additionalParams' => $additionalParamsAsStr,
        ]);

        return $link;
    }


    /**
     * @param $content
     * @return string
     */
    protected function getKey($content)
    {
        return $substKey = str_replace(['<!--', '-->'], '', $content);
    }

    /**
     * @param $content
     * @return string
     */
    protected function wrapEsiTag($content)
    {
        return '<!--esi <esi:include src="' . $content . '" />-->';
    }

    /**
     * @param $content
     * @return bool
     */
    protected function isIntObject($content)
    {
        return (boolean)preg_match('/INT_SCRIPT/', $content);
    }
}
