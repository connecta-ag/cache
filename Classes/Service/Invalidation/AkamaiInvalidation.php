<?php
namespace CAG\Cache\Service\Invalidation;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use CAG\Cache\Domain\Model\InvalidationGroup;
use CAG\Cache\Domain\Model\InvalidationRequest;
use SoapClient;
use SoapFault;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

//include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/functions_include.php';
//include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/functions_include.php';
//include_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/functions_include.php';

class AkamaiInvalidation extends \CAG\Cache\Service\Invalidation\AbstractInvalidation
{
    private $extConf;
    private $soapOptions;

    public function __construct()
    {
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cache']);
        $this->setName('Akamai');

        if (preg_match('/^6\./', TYPO3_version) === 1) {
            $this->includeVendor();
        }
    }

    public function __destruct()
    {
    }

    public function clearCacheByPids($pids)
    {
        //TODO Ermitteln der Urls für die Pid und anschließendes Invalidieren über clearCacheByPaths
    }

    public function clearCacheByPaths($paths, $recursive = false, $returnResult = false)
    {
        $invalidationItems = [];
        foreach ($paths as $path) {
            /** @var \CAG\Cache\Domain\Model\InvalidationItem  $invalidationItem */
            $invalidationItem = GeneralUtility::makeInstance(\CAG\Cache\Domain\Model\InvalidationItem::class);
            $invalidationItem->setPagePath($path);
            $invalidationItem->setRecursive($recursive);
            $invalidationItems[] = $invalidationItem;
        }
        /** @var InvalidationGroup $invalidationGroup */
        $invalidationGroup = GeneralUtility::makeInstance(\CAG\Cache\Domain\Model\InvalidationGroup::class);
        $invalidationGroup->setInvalidationItems($invalidationItems);

        if (!$recursive) {
            $response = $this->doFastPurge($paths);
            $request = new InvalidationRequest();
            $request->setResponseCode($response->getStatusCode());
            $request->setResponseMessage($response->getBody());
            $request->setRequestUri($this->extConf['akamaiPropertyToPurge']);
            $invalidationGroup->addRequest($request);
        } else {
            $this->doPurge($paths);
        }

        return [$invalidationGroup];
    }

    /**
     * Do invalidation by CCU v3 (fast purge)
     * @param $paths
     */
    private function doFastPurge($paths)
    {
        $rootDir = dirname(PATH_site);
        $client = \Akamai\Open\EdgeGrid\Client::createFromEdgeRcFile('default', $rootDir . '/.edgerc');
        // use $client just as you would \Guzzle\Http\Client
        $payload = ['hostname' => $this->extConf['akamaiPropertyToPurge'], 'objects' => $paths];
        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = $client->post('/ccu/v3/invalidate/url/production', ['json' => $payload]);

        $contentType = $response->getHeader('Content-Type');
        $body = $response->getBody();

        switch ($contentType[0]) {
            case 'application/json':
                $json = \GuzzleHttp\json_decode($body);
                break;
        }
        return $response;
    }

    /**
     * Do invalidation by CCU v3 (fast purge)
     * @param $paths
     */
    private function doPurge($paths)
    {
        $contents = $this->buildECCUXML($paths);
        $this->eccuPurgeCall($contents);
    }

    /**
     * Buil the XML
     *
     * @param array Array of URLs
     * @return string XML of flush Statement
     */
    private function buildECCUXML($paths)
    {
        $xml = [];
        $xml[] = '<?xml version="1.0"?>';
        $xml[] = '<!-- Submitted by EXT:cache-->';
        $xml[] = '<eccu>';

        foreach ($paths as $path) {
            $parsedUrl = parse_url($path);
            $path = $parsedUrl['path'];

            $pathinfo = pathinfo($path);

            $extCondition = null;
            if (!empty($pathinfo['extension']) && $pathinfo['filename'] == '*') {
                $path = $pathinfo['dirname'];
                $extCondition = $pathinfo['extension'];
            }
            $xml[] = $this->buildEccuPathWrapper($path, $extCondition);
        }
        $xml[] = '</eccu>';
        return implode(chr(10), $xml);
    }

    /**
     * PublishECCU
     * inspired by https://drupal.org/files/akamai-eccu-support-1387298-8.patch
     * @param   string      XML strucutred purge statement
     * @return  bool
     */
    private function eccuPurgeCall($contents)
    {
        $eccuWSDL = ExtensionManagementUtility::extPath('cache') . 'Resources/Private/WSDL/PublishEccu.wsdl';
        $filename                   = 'cache' . strftime('%d-%m-%Y_%H-%M-%S', time());          // xsd:string   Name of the ECCU file
        //$contents                 = $this->buildEccuXML($match_params);   // xsd:base64Binary     Contents of the ECCU file
        $notes                      = 'zoat_akamai Typo3 Extension by Hannes Maier, 2013';                      // xsd:string   User supplied optional comments
        $versionString              = time();                                                                   // xsd:string   User supplied optional project version
        $propertyName               = $this->extConf['akamaiPropertyToPurge'];                                  // xsd:string   Property to which this ECCU file will apply
        $propertyType               = 'hostheader';                                                             // xsd:string   Type of property. Must be either "hostheader" or "arltoken".
        $propertyNameExactMatch     = true;                                                                     // xsd:boolean  Set to true when match on digital property is Exact. Otherwise match will be Right-Handed Side (RHS) match. (e.g., *.example.com matches test.example.com, real.example.com, etc.)
        $statusChangeEmail          = $this->extConf['akamaiStatusChangeEmail'];                                                    // xsd:string   Email address to be notified when the status of this ECCU file changes
        $this->soapOptions['login'] = $this->extConf['akamaiSoapLogin'];
        $this->soapOptions['password'] =  $this->extConf['akamaiSoapPassword'];
        $eccuapi = new SoapClient($eccuWSDL, $this->soapOptions);
        try {
            $purgeResult = $eccuapi->upload($filename, $contents, $notes, $versionString, $propertyName, $propertyType, $propertyNameExactMatch, $statusChangeEmail);
            if ($purgeResult > 100) { // 100 = success
                $message = 'Akamai purge request succeeded. ID: ' . $purgeResult;
                GeneralUtility::sysLog($message, 'cache', GeneralUtility::SYSLOG_SEVERITY_INFO);
                return true;
            }
            $message = 'Something went wrong. Akamai purge request failed. Code: ' . $purgeResult;
            GeneralUtility::sysLog($message, 'cache', GeneralUtility::SYSLOG_SEVERITY_ERROR);
            return false;

            return false;
        } catch (SoapFault $e) {
            $message = 'Akamai Clear Cache Failed, No expression was sent. ' . $e->getMessage();
            GeneralUtility::sysLog($message, 'cache', GeneralUtility::SYSLOG_SEVERITY_ERROR);
            throw new Exception($message);
            return false;
        }
        return true;
    }

    /**
     * build ECCU EXML parts
     * @param null $fileExtension
     * @return string
     */
    private function buildEccuPathWrapper($path, $fileExtension = null)
    {
        $out = [];
        $urlParts = GeneralUtility::trimExplode('/', $path, true);
        for ($i=0; $i<count($urlParts); $i++) {
            $out[] = '<match:recursive-dirs value="' . $urlParts[$i] . '">';
        }

        $this->buildEccuFilenamePart($out, $fileExtension);

        for ($i=0; $i<count($urlParts); $i++) {
            $out[] = '</match:recursive-dirs>';
        }
        return implode(chr(10), $out);
    }

    private function buildEccuFilenamePart(&$out, $fileExtension)
    {
        if (!empty($fileExtension)) {
            $out[] = '<match:ext value="' . $fileExtension . '">';
            $out[] =    '<revalidate>now</revalidate>';
            $out[] = '</match:ext>';
        } else {
            $out[] = '<revalidate>now</revalidate>';
        }
    }

    private function includeVendor()
    {
        //      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Timestamp.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Nonce.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Exception.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Exception/ConfigException.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Exception/SignerException.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php/src/Authentication/Exception/SignerException/InvalidSignDataException.php';
//
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/ClientInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Client.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/HandlerStack.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/Proxy.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/CurlMultiHandler.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/CurlFactoryInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/CurlFactory.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/CurlHandler.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/StreamHandler.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Middleware.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/RedirectMiddleware.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/RequestOptions.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/PrepareBodyMiddleware.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/guzzle/src/Handler/EasyHandle.php';
//
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/log/Psr/Log/LoggerAwareInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/http-message/src/UriInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/http-message/src/MessageInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/http-message/src/RequestInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/http-message/src/StreamInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr/http-message/src/ResponseInterface.php';
//
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php-client/src/Handler/Authentication.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/AkamaiOPEN-edgegrid-php-client/src/Client.php';
//
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/MessageTrait.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/Stream.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/Uri.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/UriResolver.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/Request.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/psr7/src/Response.php';
//
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/TaskQueueInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/PromiseInterface.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/FulfilledPromise.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/TaskQueue.php';
//      require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('cache') . 'Classes/Vendor/promises/src/Promise.php';
    }
}
