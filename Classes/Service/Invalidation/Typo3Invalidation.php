<?php
namespace CAG\Cache\Service\Invalidation;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Typo3Invalidation extends \CAG\Cache\Service\Invalidation\AbstractInvalidation
{
    private $extConf;

    public function __construct()
    {
        if (!extension_loaded('curl')) {
            throw new \Exception('The cURL PHP Extension is required.');
        }
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cache']);
        $this->setName('TYPO3');
    }

    /**
     * Clear cache by tags
     * @param $tags
     * @return mixed|void
     */
    public function clearCacheByTags($tags)
    {
        $cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager');
        $cacheManager->flushCachesInGroupByTags('pages', $tags);
    }

    public function clearCacheByPids($pids)
    {
        // TODO: Implement clearCacheByPids() method.
    }

    public function clearCacheByPaths($paths, $recursive = false, $returnResult = false)
    {
        // TODO: Implement clearCacheByPaths() method.
    }
}
