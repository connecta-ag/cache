<?php
namespace CAG\Cache\Service\Invalidation;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use CAG\Cache\Domain\Model\InvalidationGroup;
use CAG\Cache\Domain\Model\InvalidationInterface;
use CAG\Cache\Domain\Model\InvalidationRequest;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class VarnishInvalidation extends \CAG\Cache\Service\Invalidation\AbstractInvalidation
{
    private $elements = ['pid' => [], 'path' => [], 'pathRecursive' => [], 'tag' => []];
    private $requests = [];
    private $queue;
    private $extConf;
    private $sendTags = [];
    private $returnResult;

    public function __construct()
    {
        if (!extension_loaded('curl')) {
            throw new \Exception('The cURL PHP Extension is required.');
        }
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cache']);
        $this->setName('Varnish');
    }

    public function __destruct()
    {
        if ($this->returnResult === false) {
            $this->invalidate('pid', true);
            $this->invalidate('path');
            $this->invalidate('pathRecursive');
            $this->invalidate('tag', true);

            $this->initMultiHandle();
            $this->processHandles();

            // destroy Handle which is not required anymore
            curl_multi_close($this->queue);
        }
    }

    /**
     * Clear cache by tags
     * @param $tags
     */
    public function clearCacheByTags($tags, $returnResult = false)
    {
        $this->returnResult = $returnResult;

        foreach ($tags as $tag) {
            if (array_key_exists($tag, $this->elements['tag'])) {
                continue;
            }
            /** @var \CAG\Cache\Domain\Model\InvalidationItem  $invalidationItem */
            $invalidationItem = GeneralUtility::makeInstance(\CAG\Cache\Domain\Model\InvalidationItem::class);
            $invalidationItem->setTag($tag);
            $this->elements['tag'][$tag] = $invalidationItem;
        }
    }

    /**
     * Clear cache by page id
     * @param $pids
     * @return mixed|void
     */
    public function clearCacheByPids($pids, $returnResult = false)
    {
        $this->returnResult = $returnResult;

        foreach ($pids as $pid) {
            if (array_key_exists($pid, $this->elements['pid'])) {
                continue;
            }
            /** @var \CAG\Cache\Domain\Model\InvalidationItem  $invalidationItem */
            $invalidationItem = GeneralUtility::makeInstance(\CAG\Cache\Domain\Model\InvalidationItem::class);
            $invalidationItem->setPageId($pid);
            $this->elements['pid'][$pid] = $invalidationItem;
        }
    }

    /**
     * Clear cache by path (/assets/Stylesheets/style.css)
     * @param $paths
     * @param bool $recursive
     * @return array
     */
    public function clearCacheByPaths($paths, $recursive = false, $returnResult = false)
    {
        $this->returnResult = $returnResult;

        foreach ($paths as $path) {
            /** @var \CAG\Cache\Domain\Model\InvalidationItem  $invalidationItem */
            $invalidationItem = GeneralUtility::makeInstance(\CAG\Cache\Domain\Model\InvalidationItem::class);
            $invalidationItem->setPagePath($path);
            $invalidationItem->setRecursive($recursive);
            if ($recursive) {
                $this->elements['pathRecursive'][] = $invalidationItem;
            } else {
                $this->elements['path'][] = $invalidationItem;
            }
        }

        // TODO clear queue after purge with returnResult == true
        if ($this->returnResult) {
            $this->invalidate('path');
            $this->invalidate('pathRecursive');

            $this->initMultiHandle();
            $this->processHandles();

            // destroy Handle which is not required anymore
            curl_multi_close($this->queue);
        }

        return $this->requests;
    }

    /**
     * Initiate varnish request with invalidation description as header data
     * @param $type what kind of invalidation (page id, path or path recursive)
     * @param bool $mergeElements merge invalidation description into header data merged by or condition
     */
    private function invalidate($type, $mergeElements = false)
    {
        if (count($this->elements[$type]) > 0) {
            if ($mergeElements) {
                // Merge invalidations into one request
                $invalidationGroup = new InvalidationGroup();
                $invalidationGroup->setInvalidationItems($this->elements[$type]);
                $this->sendToAllVarnishServers('/', $invalidationGroup, $this->extConf['banRequestMethod']);
            } else {
                // Send invalidations as independent request to varnish
                foreach ($this->elements[$type] as &$invalidationItem) {
                    $this->sendToAllVarnishServers('/', $invalidationItem, $this->extConf['banRequestMethod']);
                }
            }
        }
    }

    /**
     * Send invalidation to all varnish servers
     * @param $url
     * @param $invalidationHeaderName
     * @param $invalidationString
     * @param $httpMethod
     * @return array
     */
    private function sendToAllVarnishServers($invalidationPath, InvalidationInterface &$invalidationItem, $httpMethod)
    {
        $invalidationRegexp = $invalidationItem->getInvalidationRegexp();

        $header = [
            $invalidationItem->getInvalidationHeadeName() . ': ' . $invalidationRegexp,
        ];

        $invalidationPath = ltrim($invalidationPath, '/');
        $invalidationPath = '/' . $invalidationPath;

        $servers = $this->extConf['varnishServers'];

        if (!empty($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cache']['varnishServers'])) {
            if (!empty($servers)) {
                $servers .= ',';
            }

            $servers .= $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cache']['varnishServers'];
        }

        if (!empty($servers)) {
            if (!is_array($servers)) {
                $servers = explode(',', $servers);
            }

            foreach ($servers as $server) {
                $basePath = ltrim($server, '/');
                $url = $basePath . $invalidationPath;
                $request = $this->addCommand($url, $httpMethod, $header);
                $invalidationItem->addRequest($request);
                $this->requests[] = $invalidationItem;
            }
        }
    }

    /**
     * Init curl multi handle
     */
    private function initMultiHandle()
    {
        $this->queue = curl_multi_init();

        /** @var InvalidationInterface $request */
        foreach ($this->requests as $request) {
            /** @var InvalidationRequest $requestItem */
            foreach ($request->getRequests() as $requestItem) {
                curl_multi_add_handle($this->queue, $requestItem->getRequest());
            }
        }
    }

    /**
     * Create curl handles
     * @param $url
     * @param $method
     * @param array $header
     * @return string
     */
    public function addCommand($url, $method, $header = [])
    {

        //change header to array
        if (!is_array($header)) {
            $header = [$header];
        }

        $key = sha1(implode($header));
        $request = new InvalidationRequest();
        $request->setRequest(curl_init());
        $request->setRequestUri($url);
        $curlOptions = [
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => $method,
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_CONNECTTIMEOUT  => 10,
            CURLOPT_TIMEOUT         => 90,
            CURLOPT_RETURNTRANSFER  => 1,
        ];
        curl_setopt_array($request->getRequest(), $curlOptions);
        return $request;
    }

    /**
     * Process curl handles
     */
    protected function processHandles()
    {
        $running = null;
        do {
            curl_multi_exec($this->queue, $running);
        } while ($running);
    }

    private function anaylseHandles()
    {
        $errorMessage = null;
        $successMessage = null;
        /** @var InvalidationInterface $invalidationItem */
        foreach ($this->requests as &$invalidationItem) {
            $requestKey = $invalidationItem->getRequests()[0];
            $httpCode = curl_getinfo($requestKey, CURLINFO_HTTP_CODE);
            $invalidationItem->setRequestSuccess(true);
            if ($httpCode != 200) {
                $messageString = 'Die Änderungen konnten nicht an den konfigurierten Reverse-Proxy-Cache übermittelt werden!<br/>';
                if ($httpCode == 0) {
                    $error = curl_error($requestKey);
                    $messageString .= 'Curl-Fehler: ' . $error;
                } else {
                    $messageString .= 'Dieser Antwortete mit dem Statuscode: ' . $httpCode;
                }

                $invalidationItem->setRequestMessage($messageString);
            }
        }
    }
}
