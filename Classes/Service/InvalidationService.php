<?php
namespace CAG\Cache\Service;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

class InvalidationService extends \TYPO3\CMS\Core\Service\AbstractService
{
    private $extConf;

    private $cacheBackends = [];

    public function __construct()
    {
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cache']);

        $this->cacheBackends[] = GeneralUtility::makeInstance('CAG\\Cache\\Service\\Invalidation\\Typo3Invalidation');

        if (!empty($this->extConf['varnishEnable'])) {
            $this->cacheBackends[] = GeneralUtility::makeInstance('CAG\\Cache\\Service\\Invalidation\\VarnishInvalidation');
        }

        if (!empty($this->extConf['akamaiEnable'])) {
            $this->cacheBackends[] = GeneralUtility::makeInstance('CAG\\Cache\\Service\\Invalidation\\AkamaiInvalidation');
        }
    }

    public function __destruct()
    {
    }

    public function clearCacheByTags(array $tags)
    {
        foreach ($this->cacheBackends as $cacheBackend) {
            $cacheBackend->clearCacheByTags($tags);
        }
    }

    public function clearCacheByPids($pid)
    {
        foreach ($this->cacheBackends as $cacheBackend) {
            $cacheBackend->clearCacheByPids($pid);
        }
    }

    public function clearCacheByPaths($paths, $recursive = false, $returnResult = false)
    {
        $results = [];
        foreach ($this->cacheBackends as $cacheBackend) {
            $item['backend'] = $cacheBackend->getName();
            $item['results'] = $cacheBackend->clearCacheByPaths($paths, $recursive, $returnResult);
            $results[] = $item;
        }
        return $results;
    }
}
