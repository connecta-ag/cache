<?php
namespace CAG\Cache\Utility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  Copyright (c) Matthias Krams, Connecta AG 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use CAG\Cache\Service\EsiTagService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Class EsiWrap
 */
class EsiWrap
{

    /**
     * @var ContentObjectRenderer
     */
    public $cObj;

    /**
     * @var \CAG\Cache\Service\EsiTagService
     */
    protected $esiTagService;

    /**
     * EsiWrap constructor
     */
    public function __construct()
    {

        /* @var $objectManager ObjectManager */
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
    }

    /**
     * @param string $content
     * @param array $params
     * @return string
     */
    public function addEsiTags($content, $params)
    {
        $content = $this->getEsiTagService()->render($content, $this->cObj);
        return $content;
    }

    /**
     * @return EsiTagService
     */
    public function getEsiTagService()
    {
        if (is_null($this->esiTagService)) {
            try {
                $this->esiTagService = $this->objectManager->get(\CAG\Cache\Service\EsiTagService::class);
            } catch (\Exception $e) {
                echo 'EsiTagService could not be initialised: ' . $e->getCode() . $e->getMessage();
            }
        }
        return $this->esiTagService;
    }
}
