<?php
namespace CAG\Cache\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class TagViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * {@inheritdoc}
     */
    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('key', 'string', 'cache tag key', true, '');
        $this->registerArgument('value', 'string', 'cache tag value', true, '');
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        if (empty($GLOBALS['CACHE_TAGS'])) {
            $GLOBALS['CACHE_TAGS'] = [];
        }

        $tag = $arguments['key'] . '_' . $arguments['value'];

        if (!in_array($tag, $GLOBALS['CACHE_TAGS'])) {
            $GLOBALS['CACHE_TAGS'][] = $tag;

            // add cache tag and prepend namespace
            $GLOBALS['TSFE']->addCacheTags(['ext_cache_' . $tag]);
        }
    }
}
