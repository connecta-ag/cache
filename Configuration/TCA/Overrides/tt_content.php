<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$tempColumns = [
    'enable_esi' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:cache/Resources/Private/Language/locallang_db.xlf:tt_content.enable_esi',
        'onChange' => 'reload',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [
                    'No caching',
                    0
                ],
                [
                    'LLL:EXT:cache/Resources/Private/Language/locallang_db.xlf:tt_content.enable_esi.varnish',
                    1
                ],
                [
                    'LLL:EXT:cache/Resources/Private/Language/locallang_db.xlf:tt_content.enable_esi.ajax',
                    2
                ]

            ],
        ]
    ],
    'esi_cache_duration' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout',
        'displayCond' => 'FIELD:enable_esi:=:1',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [
                    'No caching',
                    0
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.1',
                    60
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.2',
                    300
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.3',
                    900
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.4',
                    1800
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.5',
                    3600
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.6',
                    14400
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.7',
                    86400
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.8',
                    172800
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.9',
                    604800
                ],
                [
                    'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.cache_timeout.I.10',
                    2678400
                ]
            ],
            'default' => '0'
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'cache_esi',
    'enable_esi, esi_cache_duration'
);
$GLOBALS['TCA']['pages']['palettes']['cache_esi']['canNotCollapse'] = 1;

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', '--div--;Caching,--palette--;LLL:EXT:cache/Resources/Private/Language/locallang_db.xlf:tt_content.palette.cache_esi;cache_esi');
