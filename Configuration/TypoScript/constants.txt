plugin.tx_cache {
    view {
        templateRootPaths.0 = EXT:cache/Resources/Private/Wka/Templates/
        partialRootPaths.0 = EXT:cache/Resources/Private/Wka/Partials/
        layoutRootPaths.0 = EXT:cache/Resources/Private/Wka/Layouts/
    }

    settings {
        esiTypeNum = 999
        replaceIntWithEsi = 0
        renderEsiRemoveTagWithOriginalContent = 1
    }
}