plugin.tx_cache {
    view {
        templateRootPaths.0 = {$plugin.tx_cache.view.templateRootPaths.0}
        partialRootPaths.0 = {$plugin.tx_cache.view.partialRootPaths.0}
        layoutRootPaths.0 = {$plugin.tx_cache.view.layoutRootPaths.0}
    }
    settings {
        esiTypeNum = {$plugin.tx_cache.settings.esiTypeNum}
        replaceIntWithEsi = {$plugin.tx_cache.settings.replaceIntWithEsi}
        renderEsiRemoveTagWithOriginalContent = {$plugin.tx_cache.settings.renderEsiRemoveTagWithOriginalContent}
    }
}

plugin.CAG\Cache\Utility\EsiWrap.includeLibs = EXT:cache/Classes/Utility/EsiWrap.php

# PostUserFunc to provide esi-tags
tt_content.stdWrap.postUserFunc = CAG\Cache\Utility\EsiWrap->addEsiTags
tt_content.stdWrap.postUserFunc.includeLibs = EXT:cache/Classes/Utility/EsiWrap.php

[globalVar = GP:type = {$plugin.tx_cache.settings.esiTypeNum}]
    tt_content.stdWrap.postUserFunc >
[global]

# Load content of ESI-Tags
varnish = PAGE
varnish {
    typeNum < plugin.tx_cache.settings.esiTypeNum

    config {
        disableAllHeaderCode = 1
        sendCacheHeaders = 0
        no_cache = 1
    }

    10 = USER
    10.userFunc = CAG\Cache\Renderer\ContentElement->render
    10.includeLibs = EXT:cache/Classes/Renderer/ContentElement.php
}

# add js to main config
page {
    includeJSFooter {
        txCache = EXT:cache/Resources/Public/JavaScript/AjaxLoadContent.js
    }
}
