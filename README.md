# README #

Diese Extension dient zur Invalidierung der im Varnish vorgehaltenen Inhalte. Diese ist an die innerhalb von TYPO3 enthaltenen Cache-Invalidierungen gekoppelt, sodass der Varnish ebenfalls bei Änderungen an Inhaltselementen auf einer Seite sowie dem seitenbasierten löschen des Cache invalidiert wird.

## Funktionen ##

* Seitenbasierte Cache Invalidierung bei
    * Löschen einer Seite aus dem Cache über das Modul "Web->Page"
    * Änderungen an einem auf einer Seite enthaltenen Inhaltselement
* Invalidierung auf Basis von Tags
	* Cache-Tagging mit Hilfe eines ViewHelpers
	* Invalidierung bei Update eines Datensatzes 
* Edge Side Includes
    * Erweiterung des tt_content Backendformulars dort kann für ein Inhaltselement folgende Einstellung vorgenommen werden:
        * Inhaltselement als ESI ausspielen
        * Cache-Dauer für ESI

## Versionen ##

* v1.6 = TYPO3 9
* v1.5 = TYPO3 7

## Konfiguration ##
Folgende Schritte müssen vorgenommen werden, um die Funktionalitäten nutzen zu können.

### TYPO3 ###

**Cache Konfiguration**

Damit das Cache-Tagging korrekt verwendet werden kann, muss die Konfiguration von cache_pages angepasst werden.


Für Datenbankbackend:
```
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pages'] = array(
    'frontend' => 'CAG\Cache\Cache\Frontend\VariableFrontend',
    'backend' => 'CAG\Cache\Cache\Backend\Typo3DatabaseBackend',
);
```

Für Redis:
```
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pages'] = array(
    'frontend' => 'CAG\Cache\Cache\Frontend\VariableFrontend',
    'backend' => 'CAG\Cache\Cache\Backend\RedisBackend',
);
```

**Varnish-Server**

In der Extensionkonfiguration müssen die Varnish-Server konfiguriert werden. Hierzu wird in dem vorgesehenen Textfeld eine kommaseparierte Liste der Varnish-Server hinterlegt. Z.B. 127.0.0.1:80,127.0.0.2,127.0.0.3:443

**Konfigurationsmöglichkeiten**

* Alle INT Scripte mit ESI ausspielen
    * **Beschreibung:** Alle vom TYPO3-Caching ausgeschlossenen INT-Scripte werden ebenfalls mit Hilfe der ESI-Tags vom Varnish-Caching ausgeschlossen. Die Cache-Dauer beträgt in diesem Fall für den Bereich des INT-Scripts 0 Sekunden.
    * **Konfigurationsparameter:** plugin.tx_cache.settings.replaceIntWithEsi = 1
* Für alle ESI-Tags zusätzlich den aktuellen Inhalt zur Kompatibilität in <esi:remove>-Tags ausspielen
    * **Beschreibung:** ESI bietet die Funktionalität Inhalte innerhalb der Tags <esi:remove></esi:remove> zu entfernen, wenn die ESI-Funktion aktiv ist.
    * **Konfigurationsparameter:** renderEsiRemoveTagWithOriginalContent = 1

**Cache-Tagging**

Bei jedem Speichern eines Datensatzes im Backend, wird eine Invalidierung auf Basis des Tabellennamens und der uid getriggert. Wurde zuvor in einem Template über den ViewHelper ein entsprechender Cache-Tag gesetzt, wird eine Invalidierung in TYPO3 & Varnish durchgeführt. 
```
<html xmlns:cache="http://typo3.org/ns/CAG/Cache/ViewHelpers" data-namespace-typo3-fluid="true">

	<cache:tag key="tx_news_domain_model_news" value="{news.uid}" />

</html>
```

### Varnish ###
**Seiten-ID & URL vor Auslieferung entfernen**

Damit die Invalidierung auf Basis der Seiten-Id, Tags oder URL korrekt funktioniert, müssen diese innerhalb der Cache-Objekte gespeichert werden. Dies wird dadurch ermöglicht, dass die Extension der Serverantwort einen Header (X-CACHE-PID, X-CACHE-TAGS) und anschließend innerhalb der Varnishkonfiguration die aktuelle URL zur Erstellung des Cache-Objektes in Form eines Headers hinzufügt wird. Dieser wird anschließend wie alle anderen Header im Varnish am Cache-Objekt vorgehalten. Über diese Information ist eine spätere Invalidierung auf Basis der Seiten-Id oder URL möglich. Damit diese nur für interne Zwecke bestimmte Header-Information nicht außerhalb sichtbar ist, muss diese in der Funktion *vcl_deliver* der Varnish-Konfiguration von der auszuliefernden Antwort an den Client entfernt werden. Dazu muss folgender Code eingefügt werden: 
```
#!c++
sub vcl_deliver {
	...
	unset resp.http.X-CACHE-URL;
	unset resp.http.X-CACHE-PID;
	unset resp.http.xkey;
	...
}

sub vcl_backend_response {
	# add url to cache object
	set beresp.http.X-CACHE-URL = bereq.url;
	
	# Hinzufügen des Keys für den späteren Soft-Ban
    if(beresp.http.X-CACHE-PID) {
        if(beresp.http.xkey) {
            set beresp.http.xkey = beresp.http.xkey + " " + beresp.http.X-CACHE-PID;
        }else {
            set beresp.http.xkey = beresp.http.X-CACHE-PID;
        }
        
    }
}
```

**Invaliderung ermöglichen**

Damit die Invalidierung überhaupt erst möglich ist, muss folgender Code der Varnishkonfiguration angefügt werden:
```
#!c++
acl purge {
    "127.0.0.1";
}

sub vcl_recv {
	...
	if (req.method == "GET" && client.ip ~ purge) {
		if(req.http.X-CACHE-PID) {
			// ban by pid
			set req.http.n-gone = xkey.softpurge(req.http.X-CACHE-PID);
			return (synth(200, "Invalidated "+req.http.n-gone+" objects with pid: " + req.http.X-CACHE-PID));
		}else if(req.http.X-CACHE-TAGS) {
			// purge by (record) tag
			set req.http.n-gone = xkey.softpurge(req.http.X-CACHE-TAGS);
			return (synth(200, "Invalidated "+req.http.n-gone+" objects with tags: " + req.http.X-CACHE-TAGS));
		}else if(req.http.X-CACHE-URL) {
			// ban by url
			ban("obj.http.X-CACHE-URL == " + req.http.X-CACHE-URL);
			return (synth(200, "Banned TYPO3 URL "+ req.http.X-CACHE-URL)) ;
		}else if(req.http.X-CACHE-URL-RERCURSIVE) {
			// ban recursive by url
			ban("obj.http.X-CACHE-URL ~ ^" + req.http.X-CACHE-URL-RERCURSIVE);
			return (synth(200, "Banned TYPO3 URL recursive"+ req.http.X-CACHE-URL-RERCURSIVE)) ;
		}
	}
	...
}
```

**ESI**

Zur Aktivierung der Edge Side Includes innerhalb des Varnish muss der Konfiguration folgendes hinzugefügt werden:
```
#!c++
sub vcl_backend_response {
   ...
   set beresp.do_esi = true;
   ...
}
```