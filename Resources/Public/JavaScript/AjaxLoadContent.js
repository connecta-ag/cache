document.addEventListener("DOMContentLoaded", function() {
	tx_cache_loadcontents();
});

function tx_cache_loadcontents() {


	var elements = $('*[data-js-action="load"]');

	if (elements.length > 0) {
		elements.each(function() {

			var patternParams = /^&[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+/gi;
			var patternType = /^[1-9]{1}[0-9]*$/gi;

			var el = $(this);
			var type = el.attr('data-type');
			var params = el.attr('data-params');

			if (patternType.test(type) && patternParams.test(params)) {

				var url = "/index.php?type=" + type + params;

				$.ajax( {
					type : "GET",
					url : url,
					async: true,
					contentType: "text/html; charset=utf-8",
					dataType: "html",
					xhrFields: {
						withCredentials: true /* We have to send the fe user cookie! */
					},
					success : function(result, textStatus, xmLHttpRequest){
						try {
							if (textStatus == 'success') {

								var resHtml = $.parseHTML(result);
								el.replaceWith(resHtml);
							}
						} catch(ex) {

						}
					},
					error: function(jqXHR, textStatus, errorThrown){

					}
				});
			}
		});
	}
}
