/**
 * Module: TYPO3/CMS/Cache/ContextMenuActions
 *
 * JavaScript to handle the click action of the "Hello World" context menu item
 * @exports TYPO3/CMS/Cache/ContextMenuActions
 */
define(['jquery',
	'TYPO3/CMS/Backend/Modal',
	'TYPO3/CMS/Backend/Notification'
], function($, Modal, Icons, Notification, Viewport) {
	'use strict';

	/**
	 * @exports TYPO3/CMS/Cache/ContextMenuActions
	 */
	var ContextMenuActions = {};

	/**
	 * Say hello
	 *
	 * @param {string} table
	 * @param {int} uid of the page
	 */
	ContextMenuActions.clearExpiredUrlsOfThisPage = function (table, uid) {
		if (table === 'pages') {
			//If needed, you can access other 'data' attributes here from $(this).data('someKey')
			//see item provider getAdditionalAttributes method to see how to pass custom data attributes

			var recursive = $(this).data('recursive');

			var message = "Do you really want to delete all expired urls of page with uid:" + uid + "?";
			if(recursive) {
				message = "Do you really want to delete all expired urls of page with uid:" + uid + " and all subpages?";
			}

			Modal.confirm('Clear urls', message)
				.on('confirm.button.ok', function() {
					$.ajax({
						url: TYPO3.settings.ajaxUrls['CacheController::invalidateExpiredRealUrlEntries'],
						data: {
							id: uid,
							recursive: recursive
						},
						type: 'post',
						cache: false
					}).done(function() {
						// a reload is used in order to restore the original behaviour
						// e.g. remove groups that are now empty because the last one in the group
						// was removed
						top.TYPO3.Notification.success('Url cache', 'The expired url entries were purged', 5);
					});
					$(this).trigger('modal-dismiss');
				})
				.on('confirm.button.cancel', function() {
					$(this).trigger('modal-dismiss');
				});
		}
	};

	return ContextMenuActions;
});
