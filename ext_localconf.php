<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

switch (TYPO3_MODE) {
    case 'FE':
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] = 'CAG\\Cache\\Hooks\\ContentPostProc->sendHeader';
        break;
    case 'BE':
        // flush cache...

        // ...by pid
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearPageCacheEval'][] = 'CAG\\Cache\\Hooks\\ClearCache->clearPageCache';

        // ...by cache_cmd
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][] = 'CAG\\Cache\\Hooks\\ClearCache->clearCacheByCmd';

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = 'CAG\\Cache\\Hooks\\DataHandler';

        break;
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
    'cache',
    'cache_invalidation',
    CAG\Cache\Service\InvalidationService::class,
    [
        'title' => 'Cache invalidation service',
        'description' => 'Cache invalidation service',
        'subtype' => 'cacheInvalidation',
        'available' => true,
        'priority' => 100,
        'quality' => 100,
        'os' => '',
        'exec' => '',
        'className' => CAG\Cache\Service\InvalidationService::class
    ]
);
