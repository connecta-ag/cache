<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Cache');

if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'CAG.' . $_EXTKEY,
        'tools',          // Main area
        'mod1',         // Name of the module
        '',             // Position of the module
        [          // Allowed controller action combinations
            'Backend' => 'overview,invalidatePid,invalidatePathByFastPurge,invalidatePath,invalidatePathRecursive,invalidateExpiredRealUrlEntries',
        ],
        [          // Additional configuration
            'access'    => 'user,group',
            'labels'    => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod.xml',
        ]
    );
}
