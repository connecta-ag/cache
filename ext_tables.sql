CREATE TABLE tt_content (
  enable_esi tinyint(4) unsigned DEFAULT '0' NOT NULL,
  esi_cache_duration int(10) DEFAULT '0' NOT NULL,
);
